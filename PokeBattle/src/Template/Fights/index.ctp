<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Fight[]|\Cake\Collection\CollectionInterface $fights
 */
?>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

<nav class="navbar sticky-top navbar-light" style="background-color: #14BDC2">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Dresseurs
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="http://localhost/PokeBattle/dresseurs">Liste des dresseurs</a>
          <a class="dropdown-item" href="http://localhost/PokeBattle/dresseurs/add">Ajouter un dresseur</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Pokémon
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="http://localhost/PokeBattle/pokes">Liste des Pokémons</a>
          <a class="dropdown-item" href="http://localhost/PokeBattle/pokes/add">Ajouter un Pokémon</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Dresseur-Pokémons
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="http://localhost/PokeBattle/dresseur-pokes">Liste des dresseur-Pokémons</a>
          <a class="dropdown-item" href="http://localhost/PokeBattle/dresseur-pokes/add">Ajouter un pokémon à un dresseur</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Combats
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="http://localhost/PokeBattle/fights">Liste des combats</a>
          <a class="dropdown-item" href="http://localhost/PokeBattle/fights/add">Lancer un combat</a>
      </li>
    </ul>
  </div>
</nav>

<h4><?= __('Combats') ?></h4>

<table class="table table-hover">
  <thead>
    <tr>
      <th scope="col"><?= $this->Paginator->sort('Id') ?></th>
      <th scope="col"><?= $this->Paginator->sort('Id du premier dresseur') ?></th>
      <th scope="col"><?= $this->Paginator->sort('Id du deuxième dresseur') ?></th>
      <th scope="col"><?= $this->Paginator->sort('Id du dresseur gagnant') ?></th>
      <th scope="col"><?= $this->Paginator->sort('Fait le :') ?></th>
      <th scope="col" class="actions"><?= __('Actions') ?></th>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($fights as $fight): ?>
    <tr>
      <td><?= $this->Number->format($fight->id) ?></td>
      <td><?= $this->Number->format($fight->first_dresseur_id) ?></td>
      <td><?= $this->Number->format($fight->second_dresseur_id) ?></td>
      <td><?= $this->Number->format($fight->winner_dresseur_id) ?></td>
      <td><?= h($fight->created) ?></td>
      <td class="actions">
        <?= $this->Html->link(__('Voir'), ['action' => 'view', $fight->id]) ?>
        <?= $this->Form->postLink(__('Supprimer'), ['action' => 'delete', $fight->id], ['confirm' => __('Etes vous sûr de supprimer {0}?', $fight->first_name)]) ?>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('premier')) ?>
            <?= $this->Paginator->prev('< ' . __('précédent')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('suivant') . ' >') ?>
            <?= $this->Paginator->last(__('dernier') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} sur {{pages}}, montrant {{current}} enregistrement(s) sur un total de {{count}}')]) ?></p>
    </div>
</div>

